        /* DB Accessor */
        var db;

        /* MN: Main page (#mainPage)
         */
        var MN = {
            listener: {

                goToAddSuspectBtnClicked: function () {
                    menu.setMainPage('addSuspect.html', {});
                },

                goToSlsBtnClicked: function () {
                    menu.setMainPage('suspectListAndSearch.html', {
                        callback: SLS.pageInit()
                    });
                },

                deleteDataBtnClicked: function () {

                    ons.notification.confirm({
                        title: "Data deletion confirmation",
                        message: "All your data will be erased, are you sure?",
                        modifier: 'material',
                        buttonLabels: ["NO", "YES, I CONFIRM"],
                        callback: function (idx) {

                            switch (idx) {

                                /* First button: NO */
                            case 0:
                                break;

                                /* Second button: CONFIRM */
                            case 1:

                                var beforeCount = 0;
                                db.transaction(function (tx) {

                                    tx.executeSql("SELECT COUNT(*) AS cnt FROM suspect;", [], function (tx, beforeRes) {

                                        beforeCount = beforeRes.rows.item(0).cnt;

                                        db.transaction(function (tx) {

                                            tx.executeSql("DROP TABLE IF EXISTS suspect");

                                            tx.executeSql("CREATE TABLE IF NOT EXISTS suspect (" +
                                                "id INTEGER PRIMARY KEY," +
                                                " name TEXT," +
                                                " iccode TEXT," +
                                                " gender TEXT," +
                                                " height INTEGER," +
                                                " weight INTEGER," +
                                                " agerange TEXT," +
                                                " vehicledesc TEXT," +
                                                " extrainfo TEXT," +
                                                " creationdate TEXT)");

                                            tx.executeSql("SELECT COUNT(*) AS cnt FROM suspect;", [], function (tx, afterRes) {
                                                AS.showNotification("Data deletion successful",
                                                    "Data check: " + afterRes.rows.item(0).cnt + " suspects found.<br/>" +
                                                    "Suspects deleted: " + beforeCount);
                                            });


                                        });


                                    });



                                });

                                break;
                            }

                        }

                    });
                },

                uploadDataBtnClicked: function () {
                    AS.showNotification("Test", "UPLOAD DATA BTN CLICKED");

                }

            }
        };

        /* AS: Add Suspect page (#addSuspectPage) */
        var AS = {

            /* Listeners */
            listener: {

                height: function (currentValue) {
                    $("body").find("#sCurrentHeight").text(currentValue);
                },
                weight: function (currentValue) {
                    $("body").find("#sCurrentWeight").text(currentValue);
                },
                addSuspectBtnClicked: function () {

                    var tmpNameIsDefined = AS.reqField.nameIsDefined();
                    var tmpGenderIsDefined = AS.reqField.genderIsDefined();

                    if (tmpNameIsDefined && tmpGenderIsDefined) {

                        var rawSuspect = AS.getRawSuspect();

                        AS.showConfirmDialog(rawSuspect);

                    } else {

                        var messageHTML = "";

                        if (!tmpNameIsDefined) {
                            messageHTML += "Please define a name";
                        }
                        if (!tmpGenderIsDefined) {

                            /* meaning that the name hasn't been defined */
                            if (messageHTML !== "") {
                                messageHTML += "<br/>";
                            }
                            messageHTML += "Please choose a gender";
                        }

                        AS.showNotification("Fields required", messageHTML);

                    }
                }

            },

            /* Getters */
            get: {

                name: function () {
                    return $("body").find("#sName").val();
                },
                iccode: function () {
                    /* Returns null if nothing selected */
                    return $("body").find("#sIdentityCode").val();
                },
                gender: function () {
                    return $("body").find("[name=sGender]:checked").val();
                },
                height: function () {
                    return $("body").find("#sHeight").val();
                },
                weight: function () {
                    return $("body").find("#sWeight").val();
                },
                agerange: function () {
                    /* Returns null if nothing selected */
                    return $("body").find("#sAgeRange").val();
                },
                vehicledesc: function () {
                    return $("body").find("#sVehicleDesc").val();
                },
                extrainfo: function () {
                    return $("body").find("#sExtraInfo").val();
                }
            },

            reqField: {

                nameIsDefined: function () {
                    var tmpName = AS.get.name();
                    return ((typeof tmpName === 'string') &&
                        (tmpName.length > 0)) ? true : false;
                },
                genderIsDefined: function () {
                    return AS.get.gender() !== undefined ? true : false;
                }
            },

            getRawSuspect: function () {
                return {
                    name: AS.get.name(),
                    iccode: AS.get.iccode(),
                    gender: AS.get.gender(),
                    height: AS.get.height(),
                    weight: AS.get.weight(),
                    agerange: AS.get.agerange(),
                    vehicledesc: AS.get.vehicledesc(),
                    extrainfo: AS.get.extrainfo()
                };
            },

            showConfirmDialog: function (rawSuspect) {

                ons.notification.confirm({
                    title: "Suspect details review:",
                    messageHTML: "Name/Desc:" + "<br/>" +
                        rawSuspect.name + "<br/><br/>" +

                        "IC Code:" + "<br/>" +
                        rawSuspect.iccode + "<br/><br/>" +

                        "Gender: " + rawSuspect.gender + "<br/>" +
                        "Est. height (cm): " + rawSuspect.height + "cm<br/>" +
                        "Est. weight (kg): " + rawSuspect.weight + "kg<br/>" +
                        "Age range: " + rawSuspect.agerange + "<br/><br/>" +

                        "Vehicle desc:" + "<br/>" +
                        msTools.transform.optField(rawSuspect.vehicledesc) + "<br/><br/>" +

                        "Additional info:" + "<br/>" +
                        msTools.transform.optField(rawSuspect.extrainfo),
                    modifier: 'material',
                    buttonLabels: ["Go back", "Confirm"],
                    callback: function (idx) {

                        switch (idx) {
                        case 0:
                            break;

                        case 1:

                            var newSuspect = new Suspect(rawSuspect);

                            db.transaction(function (tx) {

                                tx.executeSql("SELECT COUNT(*) AS cnt FROM suspect WHERE name = ?;", [newSuspect.name], function (tx, unicityCheckRes) {

                                        var count = unicityCheckRes.rows.item(0).cnt;

                                        if (count > 0) {

                                            AS.showNotification("Name already taken", "Please choose another one");

                                        } else {

                                            var valuesArray = msTools.transform.suspectToValuesArray(newSuspect);

                                            db.transaction(function (tx) {

                                                /* IMPORTANT TO PUT THE TRANSACTION 'tx' in parameter inside the SuccessCallback! */

                                                tx.executeSql("INSERT INTO suspect (name, iccode, gender, height," +
                                                    " weight, agerange, vehicledesc, extrainfo, creationdate)" +
                                                    " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)",
                                                    valuesArray,
                                                    function (tx, insertRes) {

                                                        VS.currentSuspectId = insertRes.insertId.toString();
                                                        //                                                        AS.showNotification("INSERT RESULT", "InsertId: " + insertedSuspectId);

                                                        menu.setMainPage("suspectView.html", {
                                                            callback: VS.pageInit()
                                                        });


                                                    },
                                                    function (insertErr) {
                                                        AS.showNotification("Suspect Insertion ERROR", "Nope!" + insertErr.message);
                                                    });

                                            });

                                        }

                                    },

                                    function (unicityCheckErr) {
                                        AS.showNotification("DB Error", "Name unicity check failed.<br/>" + unicityCheckErr.message);
                                    });

                            });


                            break;

                        }
                    }
                });
            },

            /* Equivalent to the Android Toast */
            showNotification: function (title, messageHTML) {
                ons.notification.alert({
                    title: title,
                    messageHTML: messageHTML,
                    animation: "fade"
                });
            },


        };

        /* VS: View Suspect page */
        var VS = {

            currentSuspectId: 0,

            set: {
                id: function (id) {
                    document.getElementById("svId").innerHTML = id;
                },
                name: function (name) {
                    document.getElementById("svName").innerHTML = name;
                },
                iccode: function (iccode) {
                    document.getElementById("svIccode").innerHTML = iccode;
                },
                gender: function (genderChar) {
                    document.getElementById("svGender").innerHTML = msTools.transform.genderToString(genderChar);
                },
                height: function (height) {
                    document.getElementById("svHeight").innerHTML = height;
                },
                weight: function (weight) {
                    document.getElementById("svWeight").innerHTML = weight;
                },
                agerange: function (agerange) {
                    document.getElementById("svAgeRange").innerHTML = agerange;
                },
                vehicledesc: function (vehicledesc) {
                    document.getElementById("svVehicleDesc").innerHTML = vehicledesc;
                },
                extrainfo: function (extrainfo) {
                    document.getElementById("svExtraInfo").innerHTML = extrainfo;
                }
            },

            feedPage: function (suspect) {
                VS.set.id(suspect.id);
                VS.set.name(suspect.name);
                VS.set.iccode(suspect.iccode);
                VS.set.gender(suspect.gender);
                VS.set.height(suspect.height);
                VS.set.weight(suspect.weight);
                VS.set.agerange(suspect.agerange);
                VS.set.vehicledesc(msTools.transform.optField(suspect.vehicledesc));
                VS.set.extrainfo(msTools.transform.optField(suspect.extrainfo));

            },

            pageInit: function () {
                /* Finds the inserted Suspect and feed the page */

                var insertedSuspectId = VS.currentSuspectId;
                db.transaction(function (tx) {

                    tx.executeSql("SELECT * FROM suspect WHERE id = ?", [insertedSuspectId], function (tx, selectRes) {

                        var insertedSuspect = new Suspect();
                        insertedSuspect.id = selectRes.rows.item(0).id;
                        insertedSuspect.name = selectRes.rows.item(0).name;
                        insertedSuspect.iccode = selectRes.rows.item(0).iccode;
                        insertedSuspect.gender = selectRes.rows.item(0).gender;
                        insertedSuspect.height = selectRes.rows.item(0).height;
                        insertedSuspect.weight = selectRes.rows.item(0).weight;
                        insertedSuspect.agerange = selectRes.rows.item(0).agerange;
                        insertedSuspect.vehicledesc = selectRes.rows.item(0).vehicledesc;
                        insertedSuspect.extrainfo = selectRes.rows.item(0).extrainfo;
                        insertedSuspect.creationdate = selectRes.rows.item(0).creationdate;

                        VS.feedPage(insertedSuspect);

                        /* If finding the inserted suspect returns an error */
                    }, function (err) {
                        AS.showNotification("FIND INSERTED SUSPECT ERROR", "Message: " + err.message);
                    });

                });
            }

        };

        /* SLS: Suspect List and Search (#slsPage) */
        var SLS = {

            listener: {
                sItemClicked: function (sItem) {
                    var suspectId = sItem.querySelector(".slsId").innerHTML;
                    VS.currentSuspectId = suspectId;
                    menu.setMainPage("suspectView.html", {
                        callback: VS.pageInit()
                    });

                }
            },

            feedPage: function (suspectsArray) {

                if (suspectsArray.length > 0) {

                    var sCount = suspectsArray.length;

                    for (var i = 0; i < sCount; i++) {

                        var sItem = $("<ons-list-item class='suspectItem' modifier='chevron' onClick='SLS.listener.sItemClicked(this)'>" +
                            "<ons-icon icon='fa-user' size='20px'></ons-icon>" +
                            "<span class='slsName'>" + suspectsArray[i].name + "</span>" +
                            "<span class='slsId' hidden>" + suspectsArray[i].id + "</span>" +
                            "</ons-list-item>");
                        sItem.appendTo($("#slsAnchor"));
                        ons.compile(sItem[0]);
                    }

                } else {

                    var noDataItem = $("<ons-list-item class='noDataItem' modifier='inset'>" +
                        "<span class='txtCenter'>No data available</span>" +
                        "</ons-list-item>");
                    noDataItem.appendTo($("#slsAnchor"));
                    ons.compile(noDataItem[0]);

                }

            },

            pageInit: function () {

                db.transaction(function (tx) {

                    tx.executeSql("SELECT id, name FROM suspect", [], function (tx, allSuspectsRes) {


                        var suspectsArray = [];
                        var sCount = allSuspectsRes.rows.length;


                        if (sCount > 0) {
                            
                            /* Array initialization */
                            for (var j = 0; j < sCount; j++) {
                                suspectsArray.push({
                                    id: allSuspectsRes.rows.item(j).id,
                                    name: allSuspectsRes.rows.item(j).name
                                });
                            }
                            
                            /* Loop on array to generate items */
                            for (var k = 0; k < sCount; k++) {

                                var sItem = $("<ons-list-item class='suspectItem' modifier='chevron' onClick='SLS.listener.sItemClicked(this)'>" +
                                    "<ons-icon icon='fa-user' size='20px'></ons-icon>" +
                                    "<span class='slsName'>" + suspectsArray[k].name + "</span>" +
                                    "<span class='slsId' hidden>" + suspectsArray[k].id + "</span>" +
                                    "</ons-list-item>");
                                sItem.appendTo($("#slsAnchor"));
                                ons.compile(sItem[0]);
                            }
                            
                        } else {

                            var noDataItem = $("<ons-list-item class='noDataItem' modifier='inset'>" +
                                "<span class='txtCenter'>No data available</span>" +
                                "</ons-list-item>");
                            noDataItem.appendTo($("#slsAnchor"));
                            ons.compile(noDataItem[0]);

                        }


                    }, function (err) {
                        AS.showNotification("SELECT ALL SUSPECTS ERROR",
                            "Retrievinfg all the suspects failed:<br/>" +
                            err.message);
                    });

                });

            }


        };

        document.addEventListener('deviceready', onDeviceReady.bind(this), false);

        function onDeviceReady() {
            // Handle the Cordova pause and resume events
            document.addEventListener('pause', onPause.bind(this), false);
            document.addEventListener('resume', onResume.bind(this), false);

            /* DB Connection Test */
            db = window.sqlitePlugin.openDatabase({
                name: "madsleuth.db"
            }, function (db) {

                db.transaction(function (tx) {
                    console.log("Open Database OK");
                    document.getElementById('goToAddSuspectBtn').innerHTML += "<br/>DB OK";
                }, function (err) {
                    console.log('Open database ERROR: ' + JSON.stringify(err));
                    document.getElementById('uploadDataBtn').innerHTML += "<br/>DB NOT OK";

                });
            });


            db.transaction(function (tx) {
                tx.executeSql("DROP TABLE IF EXISTS suspect");

                tx.executeSql("CREATE TABLE IF NOT EXISTS suspect (" +
                    "id INTEGER PRIMARY KEY," +
                    " name TEXT," +
                    " iccode TEXT," +
                    " gender TEXT," +
                    " height INTEGER," +
                    " weight INTEGER," +
                    " agerange TEXT," +
                    " vehicledesc TEXT," +
                    " extrainfo TEXT," +
                    " creationdate TEXT)");

                tx.executeSql("SELECT COUNT(*) AS cnt FROM suspect;", [], function (tx, res) {
                    ons.notification.alert({
                        message: "Init Count: " + res.rows.item(0).cnt
                    });
                });

            });


        };


        function onPause() {
            // TODO: This application has been suspended. Save application state here.
        };

        function onResume() {
            // TODO: This application has been reactivated. Restore application state here.
        };