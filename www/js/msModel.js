    /**
     * [[Suspect constructor]]
     * @param {Object} rawSuspect [[Object containing the data from the addSuspect form]]
     */
    var Suspect = function(rawSuspect) {
        
        if(rawSuspect === undefined) {
            this.id = 0;
            this.name = "";
            this.iccode = "";
            this.gender = "";
            this.height = 0;
            this.weight = 0;
            this.agerange = "";
            this.vehicledesc = "";
            this.extrainfo = "";
            this.creationdate = ""; 
        } else {
            this.id = 0;
            this.name = rawSuspect.name;
            this.iccode = rawSuspect.iccode;
            this.gender = msTools.transform.genderToChar(rawSuspect.gender);
            this.height = rawSuspect.height;
            this.weight = rawSuspect.weight;
            this.agerange = rawSuspect.agerange;
            this.vehicledesc = rawSuspect.vehicledesc;
            this.extrainfo = rawSuspect.extrainfo;

            // Equivalent to: new Date(), to String
            this.creationdate = moment().format("DD/MM/YYYY, HH:mm:ss"); 
        }
        
        Suspect.prototype.toString = function toString() {
            return this.id + ", " + this.name + ", " + this.iccode + ", " +
                this.gender + ", " + this.height + ", " + this.weight + ", " +
                this.agerange + ", " + this.vehicledesc + ", " + this.extrainfo + ", " +
                this.creationdate;
        }
     
    }
