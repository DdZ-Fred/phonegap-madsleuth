// For an introduction to the Blank template, see the following documentation:
// http://go.microsoft.com/fwlink/?LinkID=397704
// To debug code on page load in Ripple or on Android devices/emulators: launch your app, set breakpoints,
// and then run "window.location.reload()" in the JavaScript Console.
(function () {
    "use strict";

    document.addEventListener('deviceready', onDeviceReady.bind(this), false);

    var suspectSQL = {

        const: {
            tableName: "suspect",
            column0_id: "id",
            column1_name: "name",
            column2_icCode: "iccode",
            column3_gender: "gender",
            column4_height: "height",
            column5_weight: "weight",
            column6_ageRange: "agerange",
            column7_vehicleDesc: "vehicledesc",
            column8_extraInfo: "extrainfo",
            column9_creationData: "creationdate"
        },

        createTable: "CREATE TABLE IF NOT EXISTS " + suspectSQL.const.tableName + " (" +
            suspectSQL.const.column0_id + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            suspectSQL.const.column1_name + " TEXT, " +
            suspectSQL.const.column2_icCode + " TEXT, " +
            suspectSQL.const.column3_gender + " TEXT, " +
            suspectSQL.const.column4_height + " INTEGER, " +
            suspectSQL.const.column5_weight + " INTEGER, " +
            suspectSQL.const.column6_ageRange + " TEXT, " +
            suspectSQL.const.column7_vehicleDesc + " TEXT, " +
            suspectSQL.const.column8_extraInfo + " TEXT, " +
            suspectSQL.const.column9_creationData + " TEXT)"


    }

    function onDeviceReady() {
        // Handle the Cordova pause and resume events
        document.addEventListener('pause', onPause.bind(this), false);
        document.addEventListener('resume', onResume.bind(this), false);

        document.getElementById('sCurrentHeight').innerHTML = "GIBIDIN";
        
        
        /* DB Connection Test */
        var db = window.sqlitePlugin.openDatabase({
            name: "madsleuth.db"
        }, function (db) {

            document.getElementById('sCurrentHeight').innerHTML = "GIBIDIN 2";

            
            db.executeSql(suspectSQL.createTable, [], function (res) {
                alert("Suspect table creation: " + res.rows.item(0));
            });

            db.executeSql("select count('id') from suspect as scount", [], function (res) {
                var sCount = res.rows.item(0).scount;
                document.getElementById('sCurrentHeight').innerHTML = sCount;
            });
        });


    };

    function onPause() {
        // TODO: This application has been suspended. Save application state here.
    };

    function onResume() {
        // TODO: This application has been reactivated. Restore application state here.
    };


})();