var AS = {

    /* Listeners */
    listener: {

        height: function (currentVal) {
            $("body").find("#sCurrentHeight").text(currentVal);
        },
        weight: function (currentVal) {
            $("body").find("#sCurrentWeight").text(currentVal);
        }

    },

    /* Getters */
    get: {

        name: function () {
            return $("body").find("#sName").val();
        },
        icCode: function () {
            /* Returns null if nothing selected */
            return $("body").find("#sIdentityCode").val();
        },
        gender: function () {
            return $("body").find("[name=sGender]:checked").val();
        },
        height: function () {
            return $("body").find("#sHeight").val();
        },
        weight: function () {
            return $("body").find("#sWeight").val();
        },
        ageRange: function () {
            /* Returns null if nothing selected */
            return $("body").find("#sAgeRange").val();
        },
        vehicleDesc: function () {
            return $("body").find("#sVehicleDesc").val();
        },
        extraInfo: function () {
            return $("body").find("#sExtraInfo").val();
        }
    },
    
    /* Transformation functions */
    transform: {
        optField: function(fieldVal) {
            return fieldVal === "" ? "No description given": fieldVal;
        }
    },

    reqField: {
        
        nameIsDefined: function() {
            var tmpName = AS.get.name();
            return ((typeof tmpName === 'string')
                    &&
                    (tmpName.length > 0)) ? true : false;
        },
        genderIsDefined: function() {
            return AS.get.gender() !== undefined ? true : false;
        }
    },
    
    getSuspectDetails: function () {
        return {
            name: AS.get.name(),
            icCode: AS.get.icCode(),
            gender: AS.get.gender(),
            height: AS.get.height(),
            weight: AS.get.weight(),
            ageRange: AS.get.ageRange(),
            vehicleDesc: AS.get.vehicleDesc(),
            extraInfo: AS.get.extraInfo()
        };
    },

    showConfirmDialog: function(suspect) {
        
        ons.notification.confirm({
                    title: "Suspect details review:",
                    messageHTML: 
                        "Name/Desc:" + "<br/>" +
                        suspect.name + "<br/><br/>" +
                    
                        "IC Code:" + "<br/>" +
                        suspect.icCode + "<br/><br/>" +
                        
                        "Gender: " + suspect.gender + "<br/>" +
                        "Est. height (cm): " + suspect.height + "cm<br/>" +
                        "Est. weight (kg): " + suspect.weight + "kg<br/>" +
                        "Age range: " + suspect.ageRange + "<br/><br/>" +
                    
                        "Vehicle desc:" + "<br/>" +
                        AS.transform.optField(suspect.vehicleDesc) + "<br/><br/>" +
                    
                        "Additional info:" + "<br/>" +
                        AS.transform.optField(suspect.extraInfo),
                    modifier: 'material',
                    buttonLabels: ["Go back", "Confirm"],
                    callback: function(idx) {
                        switch(idx) {
                            case 0:
                                break;
                                
                            case 1:
                                ons.notification.alert({
                                    message: 'You pressed Confirm.',
                                    modifier: 'material'
                                });
                                break;
                                
                        }
                    }
                });
    },
    
    /* Equivalent to the Android Toast */
    showNotification: function(messageHTML) {
        ons.notification.alert({
            title: "Fields required",
            messageHTML: messageHTML,
            animation: "fade"
        });
    },
    
    addBtnEvent: function() {
        
        var tmpNameIsDefined = AS.reqField.nameIsDefined();
        var tmpGenderIsDefined = AS.reqField.genderIsDefined();
        
        if(tmpNameIsDefined && tmpGenderIsDefined) {
            
            var suspect = AS.getSuspectDetails();
            
            AS.showConfirmDialog(suspect);
            
        } else {
            
            var messageHTML = "";
            
            if(!tmpNameIsDefined) {
                messageHTML += "Please define a name";
            }
            if(!tmpGenderIsDefined) {
                
                /* meaning that the name hasn't been defined */
                if(messageHTML !== "") {
                    messageHTML += "<br/>";
                }
                messageHTML += "Please choose a gender";
            }
            
            AS.showNotification(messageHTML);
            
        }
    }

}