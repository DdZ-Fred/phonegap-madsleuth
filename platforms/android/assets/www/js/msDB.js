/**********************************
 * SUSPECT TABLE SQL HELPER (VARS)
 * ********************************/

var SQL_Suspect = {

    table: {
        name: 'suspect',
        col0_id: 'id',
        col1_name: 'name',
        col2_iccode: 'iccode',
        col3_gender: 'gender',
        col4_height: 'height',
        col5_weight: 'weight',
        col6_agerange: 'agerange',
        col7_vehicledesc: 'vehicledesc',
        col8_extrainfo: 'extrainfo',
        col9_creationdate: 'creationdate'
    },

    query: {

        createTable: function() { 
            
            db.executeSql("CREATE TABLE IF NOT EXISTS suspect (" +
                            "id INTEGER PRIMARY KEY," +
                            " name TEXT," +
                            " iccode TEXT," +
                            " gender TEXT," +
                            " height INTEGER," +
                            " weight INTEGER," +
                            " agerange TEXT," +
                            " vehicledesc TEXT," +
                            " extrainfo TEXT," +
                            " creationdate TEXT)"
                         );
        },

        /* For addSuspectPage */
        nameIsUniqueA: "SELECT COUNT(*)" +
            " FROM " + SQL_Suspect.table.name +
            " WHERE " + SQL_Suspect.table.col1_name + " = ?",
        
        /* For EditSuspectPage */
        nameIsUniqueE: "SELECT " + SQL_Suspect.table.col0_id + 
            " FROM " + SQL_Suspect.table.name +
            " WHERE " + SQL_Suspect.table.col1_name + " = ?",

        /* Takes in parameter a real Suspect object using the class con structor */
        insert: "INSERT INTO " + SQL_Suspect.table.name + " (" +
            SQL_Suspect.table.col1_name + ", " +
            SQL_Suspect.table.col2_iccode + ", " +
            SQL_Suspect.table.col3_gender + ", " +
            SQL_Suspect.table.col4_height + ", " +
            SQL_Suspect.table.col5_weight + ", " +
            SQL_Suspect.table.col6_agerange + ", " +
            SQL_Suspect.table.col7_vehicledesc + ", " +
            SQL_Suspect.table.col8_extrainfo + ", " +
            SQL_Suspect.table.col9_creationdate + ") " +
            "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)",

        findById: "SELECT * FROM " + SQL_Suspect.table.name + " WHERE id = ?",

        findAll: "SELECT * FROM " + SQL_Suspect.table.name,

        delete: "DELETE FROM " + SQL_Suspect.table.name + " WHERE id = ?",

        dropTable: function() {
            
            db.executeSql("DROP TABLE IF EXISTS suspect");
            
        } 
    }

}