    var msTools = {

        /* Gathers transformation functions */
        transform: {

            /**
             * [[Used with the Optional Fields: if a field is empty then it returns the String stated below]]
             * @param   {[[String]]} fieldVal [[field that can be empty]]
             * @returns {[[String]]} [[Whether the string in parameter or...]]
             */
            optField: function (fieldVal) {
                return fieldVal === "" ? "No description given" : fieldVal;
            },


            /**
             * [[Returns the gender into its full-text format]]
             * @param {[[Type]]} genderChar [[char representing a gender (m/male, f/female and u/unknown)]]
             */
            genderToString: function (genderChar) {

                switch (genderChar) {
                case 'm':
                    return "male";
                case 'f':
                    return "female";
                case 'u':
                    return "unknown";
                default:
                    return "unknown";
                }

            },


            /**
             * [[Transform the full-text gender into the appropriate char]]
             * @param   {String}   genderStr [[Full string gender: male/female/unknown]]
             * @returns {[[Type]]} [[The gender in char format: male/m, female/f and unknown/u]]
             */
            genderToChar: function (genderStr) {
                return genderStr.charAt(0).toLowerCase();
            },


            /**
             * [[Prepare the Suspect object for db insertion]]
             * @param   {Suspect} suspect [[Suspect object]]
             * @returns {Array}  [[...which contains the values that will be inserted into the DB]]
             */
            suspectToValuesArray: function (suspect) {

                /* Creation date format: DD/MM/YYYY, HH:mm:ss 
                 * Gender is in char format */
                return [
                suspect.name,
                suspect.iccode,
                suspect.gender,
                suspect.height,
                suspect.weight,
                suspect.agerange,
                suspect.vehicledesc,
                suspect.extrainfo,
                suspect.creationdate
                ];

            }

        },

        defaultDateFmt: "DD/MM/YYYY, HH:mm:ss",

    };